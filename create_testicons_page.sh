displayIcons()
{
	for size in 16x16 24x24 32x32 48x48
	do
		echo "<h4>Size $size</h4>"
		echo "<div>"
	cd $size
		for category in actions apps mimetypes places
		do
			echo "<h5>$category</h5>"
			echo "<div>"
			cd $category
			for file in *.png
			do
				label=`basename $file .png`
				echo "<div class=\"icon\"><img src=\"$size/$category/$file\" title=\"$file\" /> <span>$label</span></div>"
			done
			cd ..
			echo "</div>"
		done
		cd ..
		echo "</div>"
	done
}


echo "<html><head><style type=\"text/css\">.icon { display: inline-block; width: 250px; font-size: 8pt; font-family: verdana; padding: 4px; } .icon img, .icon span { vertical-align: middle; }</style></head><body>"
echo "<div style=\"background-color: white\">"
displayIcons
echo "</div>"
echo "<div style=\"background-color: lightgray\">"
displayIcons
echo "</div>"
echo "<div style=\"background-color: darkgray\">"
displayIcons
echo "</div>"
echo "<div style=\"background-color: gray\">"
displayIcons
echo "</div>"
echo "<div style=\"background-color: orange\">"
displayIcons
echo "</div>"
echo "</body></html>"
