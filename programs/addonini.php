;<?php/*

[general]
name                        ="icons_oxygen"
version                     ="0.7.0"
encoding                    ="UTF-8"
description                 ="An icon theme for ovidentia based on the Oxygen icon theme ( https://github.com/KDE/oxygen-icons ) and Faience ( https://code.google.com/p/faience-theme/ )"
description.fr              ="Thème d'icônes basé sur Oxygen et Faience"
long_description.fr         ="README.md"
delete                      =1
ov_version                  ="6.7.5"
php_version                 ="5.1.0"
addon_access_control        ="0"
mysql_character_set_database="latin1,utf8"
author                      ="Laurent Choulette (laurent.choulette@cantico.fr) (see AUTHORS.oxygen and AUTHORS.faience files for original Icon Themes authors)"
icon                        ="oxygen.png"
configuration_page          ="preview"
tags                        ="library,default,icons"
;*/?>