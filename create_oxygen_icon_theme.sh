#!/bin/bash

BASE_DIR=${TMPDIR:-/tmp}
SVN_DIR=${BASE_DIR}/oxygen-icons
DEST_DIR=${BASE_DIR}/oxygen-icons/ovidentia

createSkeleton()
{
	mkdir -p -m777 "${SVN_DIR}"
	mkdir -p -m777 "${DEST_DIR}"
	[ -d "${DEST_DIR}/oxygen-icons" ] && rm -rf "${DEST_DIR}/oxygen-icons"
	for size in 16x16 22x22 32x32 48x48
	do
		for class in actions apps categories mimetypes objects places status
		do
			mkdir -p -m777 "${DEST_DIR}/oxygen-icons/${size}/${class}"
		done
	done
}


checkOut()
{
	cd "${SVN_DIR}"
	if [ -d oxygen-icons ]
	then
		svn up svn://anonsvn.kde.org/home/kde/trunk/kdesupport/oxygen-icons
	else
		svn co svn://anonsvn.kde.org/home/kde/trunk/kdesupport/oxygen-icons
	fi
}


copyIcon()
{
	for size in 16x16 22x22 32x32 48x48
	do
		src="${SVN_DIR}/oxygen-icons/${size}/${1}.png"
		dest="${DEST_DIR}/oxygen-icons/${size}/${2}.png"
		if [ -f "${src}" ]
		then
			cp "${src}" "${dest}"
		else
			echo "Missing source file : '${src}'"
		fi
	done
}


function copyIcons()
{
	# apps
	copyIcon "actions/view-pim-calendar"				"apps/calendar"
	copyIcon "apps/preferences-contact-list"			"apps/directories"
	copyIcon "apps/knotes"								"apps/notes"
	copyIcon "apps/accessories-text-editor"				"apps/editor"
	copyIcon "apps/system-file-manager"					"apps/file-manager"
	copyIcon "apps/kchart"								"apps/statistics"
	copyIcon "categories/applications-toys"				"apps/vacations"
	copyIcon "devices/camera-photo"						"apps/photo"
	copyIcon "categories/system-help"					"apps/faqs"
	copyIcon "actions/view-pim-contacts"				"apps/contacts"
	copyIcon "actions/dialog-ok-apply"					"apps/approbations"
	copyIcon "actions/code-class"						"apps/orgcharts"
	copyIcon "actions/irc-voice"						"apps/forums"
	copyIcon "apps/kthesaurus"							"apps/thesaurus"
	copyIcon "actions/irc-operator"						"apps/delegations"
	copyIcon "actions/view-right-new"					"apps/sections"
	copyIcon "actions/view-pim-summary"					"apps/summary"
	copyIcon "actions/view-time-schedule"				"apps/task-manager"

	copyIcon "categories/applications-office"			"apps/articles"
	copyIcon "places/mail-message"						"apps/mail"
	copyIcon "actions/system-search"					"apps/preferences-search-engine"
	copyIcon "actions/mail-send"						"apps/preferences-mail-server"
	copyIcon "apps/preferences-desktop-user"			"apps/preferences-user"
	copyIcon "apps/preferences-system-time"				"apps/preferences-date-time-format"
	copyIcon "apps/preferences-desktop-cryptography"	"apps/preferences-authentication"
	copyIcon "categories/preferences-system-network"	"apps/preferences-site"
	copyIcon "places/network-workgroup"					"apps/preferences-webservices"
	copyIcon "mimetypes/text-rdf"						"apps/preferences-wysiwyg-editor"
	copyIcon "places/folder-downloads"					"apps/preferences-upload"
	copyIcon "apps/accessories-calculator"				"apps/calculator"
	
	copyIcon "actions/view-pim-calendar"				"apps/preferences-calendar"

	copyIcon "places/user-identity"						"apps/users"
	copyIcon "apps/system-users"						"apps/groups"

	copyIcon "mimetypes/application-rss+xml"			"apps/rss"
	copyIcon "actions/im-facebook"						"apps/facebook"
	copyIcon "actions/im-twitter"						"apps/twitter"
	copyIcon "mimetypes/image-x-generic"				"apps/flickr"

	# actions
	copyIcon "actions/list-add"							"actions/list-add"
	copyIcon "actions/list-remove"						"actions/list-remove"
	copyIcon "actions/list-add-user"					"actions/list-add-user"
	copyIcon "actions/list-remove-user"					"actions/list-remove-user"
	copyIcon "actions/dialog-cancel"					"actions/dialog-cancel"
	copyIcon "actions/dialog-ok"						"actions/dialog-ok"
	copyIcon "actions/document-new"						"actions/document-new"
	copyIcon "actions/document-new"						"actions/article-new"
	copyIcon "actions/bookmark-new"						"actions/article-topic-new"
	copyIcon "actions/folder-new"						"actions/article-category-new"
	copyIcon "actions/list-add-user"					"actions/user-new"
	copyIcon "actions/edit-find-user"					"actions/edit-find-user"
	copyIcon "actions/user-properties"					"actions/user-properties"
	copyIcon "actions/folder-new"						"actions/folder-new"
	copyIcon "actions/window-new"						"actions/event-new"
	copyIcon "actions/document-new"						"actions/note-new"
	copyIcon "actions/document-edit"					"actions/document-edit"
	copyIcon "actions/user-group-new"					"actions/user-group-new"
	copyIcon "actions/user-group-delete"				"actions/user-group-delete"
	copyIcon "actions/user-group-properties"			"actions/user-group-properties"
	copyIcon "actions/edit-cut"							"actions/edit-cut"
	copyIcon "actions/edit-copy"						"actions/edit-copy"
	copyIcon "actions/edit-paste"						"actions/edit-paste"
	copyIcon "actions/edit-find"						"actions/edit-find"
	copyIcon "actions/edit-delete"						"actions/edit-delete"
	copyIcon "actions/document-save"					"actions/document-save"
	copyIcon "actions/document-print"					"actions/document-print"
	copyIcon "actions/document-properties"				"actions/document-properties"
	copyIcon "actions/go-home"							"actions/go-home"
	copyIcon "actions/go-up"							"actions/go-up"
	copyIcon "actions/go-down"							"actions/go-down"
	copyIcon "actions/go-previous"						"actions/go-previous"
	copyIcon "actions/go-next"							"actions/go-next"
	copyIcon "actions/go-first"							"actions/go-first"
	copyIcon "actions/go-last"							"actions/go-last"

	copyIcon "actions/mail-send"						"actions/mail-send"

	copyIcon "actions/view-pim-summary"					"actions/view-pim-summary"
	copyIcon "actions/view-pim-calendar"				"actions/view-pim-calendar"
	copyIcon "actions/view-pim-journal"					"actions/view-pim-journal"
	copyIcon "actions/view-pim-notes"					"actions/view-pim-notes"
	copyIcon "actions/view-pim-news"					"actions/view-pim-news"
	copyIcon "actions/view-pim-mail"					"actions/view-pim-mail"
	copyIcon "actions/view-pim-tasks"					"actions/view-pim-tasks"

	copyIcon "actions/view-refresh"						"actions/view-refresh"
	copyIcon "actions/view-calendar-day"				"actions/view-calendar-day"
	copyIcon "actions/view-calendar-week"				"actions/view-calendar-week"
	copyIcon "actions/view-calendar-workweek"			"actions/view-calendar-workweek"
	copyIcon "actions/view-calendar-month"				"actions/view-calendar-month"
	copyIcon "actions/view-calendar-timeline"			"actions/view-calendar-timeline"
	copyIcon "actions/view-calendar-list"				"actions/view-calendar-list"
	
	copyIcon "actions/view-list-text"					"actions/view-list-text"
	copyIcon "actions/view-list-tree"					"actions/view-list-tree"
	copyIcon "actions/view-list-details"				"actions/view-list-details"
	copyIcon "actions/view-history"						"actions/view-history"

	copyIcon "actions/zoom-in"							"actions/zoom-in"
	copyIcon "actions/zoom-out"							"actions/zoom-out"
	copyIcon "actions/zoom-original"					"actions/zoom-original"
	copyIcon "actions/zoom-fit-width"					"actions/zoom-fit-width"
	copyIcon "actions/zoom-fit-height"					"actions/zoom-fit-height"
	copyIcon "actions/zoom-fit-best"					"actions/zoom-fit-best"
	copyIcon "actions/help-contents"					"actions/help"
	copyIcon "status/dialog-password"					"actions/set-access-rights"
	
	copyIcon "actions/arrow-left"						"actions/arrow-left"
	copyIcon "actions/arrow-right"						"actions/arrow-right"
	copyIcon "actions/arrow-up"							"actions/arrow-up"
	copyIcon "actions/arrow-down"						"actions/arrow-down"
	copyIcon "actions/arrow-left-double"				"actions/arrow-left-double"
	copyIcon "actions/arrow-right-double"				"actions/arrow-right-double"
	copyIcon "actions/arrow-up-double"					"actions/arrow-up-double"
	copyIcon "actions/arrow-down-double"				"actions/arrow-down-double"
	
	copyIcon "actions/svn-update"						"actions/document-download"
	copyIcon "actions/svn-commit"						"actions/document-upload"
	
	copyIcon "apps/utilities-file-archiver"				"actions/archive-create"
	copyIcon "mimetypes/application-x-7z-compressed"	"actions/archive-extract"
	

	# mimetypes
	copyIcon "mimetypes/application-pgp-signature"		"mimetypes/signature"
	copyIcon "mimetypes/application-pdf"				"mimetypes/application-pdf"
	copyIcon "mimetypes/audio-x-generic"				"mimetypes/audio-x-generic"
	copyIcon "mimetypes/image-x-generic"				"mimetypes/image-x-generic"
	copyIcon "mimetypes/application-x-compress"			"mimetypes/package-x-generic"
	copyIcon "mimetypes/text-html"						"mimetypes/text-html"
	copyIcon "mimetypes/text-x-generic"					"mimetypes/text-x-generic"
	copyIcon "mimetypes/video-x-generic"				"mimetypes/video-x-generic"
	copyIcon "mimetypes/application-vnd.oasis.opendocument.text"			"mimetypes/x-office-document"
	copyIcon "mimetypes/application-vnd.oasis.opendocument.presentation"	"mimetypes/x-office-presentation"
	copyIcon "mimetypes/application-vnd.oasis.opendocument.spreadsheet"		"mimetypes/x-office-spreadsheet"
	copyIcon "mimetypes/unknown"						"mimetypes/unknown"

	# objects
	copyIcon "actions/mail-signed"						"objects/invoice"
	copyIcon "actions/document-edit-sign"				"objects/contract"
	copyIcon "actions/view-pim-contacts"				"objects/contact"
	copyIcon "places/user-identity"						"objects/user"
	copyIcon "apps/system-users"						"objects/group"
	copyIcon "actions/feed-subscribe"					"objects/tag"
	copyIcon "apps/knotes"								"objects/note"
	copyIcon "actions/mail-mark-unread"					"objects/email"
	copyIcon "mimetypes/text-rdf"						"objects/publication-article"
	copyIcon "actions/documentation"					"objects/publication-topic"
	copyIcon "places/folder-bookmark"					"objects/publication-category"
	
	copyIcon "actions/resource-group"					"objects/organization"
	copyIcon "actions/mail-receive"						"objects/shopping-cart"

	# places
	copyIcon "places/folder"							"places/folder"
	copyIcon "places/folder-red"						"places/folder-red"
	copyIcon "places/folder-favorites"					"places/folder-bookmarks"
	copyIcon "places/user-home"							"places/user-home"	
	copyIcon "places/user-trash"						"places/user-trash"
	copyIcon "places/mail-folder-inbox"					"places/mail-folder-inbox"

	#category
	copyIcon "categories/applications-education"		"categories/applications-education"
	copyIcon "categories/preferences-desktop"			"categories/preferences-desktop"
	copyIcon "categories/preferences-other"				"categories/preferences-other"


	#status
	copyIcon "status/dialog-error"						"status/dialog-error"
	copyIcon "status/dialog-information"				"status/dialog-information"
	### Question still missing
	copyIcon "categories/system-help"					"status/dialog-question"
	copyIcon "status/dialog-warning"					"status/dialog-warning"
	copyIcon "status/dialog-password"					"status/dialog-password"
	
	copyIcon "actions/edit-clear-history"				"status/content-loading"
}


createCssFiles()
{
	cd "${DEST_DIR}/oxygen-icons"

	[ -d "22x22" ] && mv "22x22" "24x24"

	for iconsize in "16x16" "24x24" "32x32" "48x48"
	do
		echo "${iconsize}"
		cd  "${iconsize}"

		cat /dev/null > icons.css
		for icontype in *
		do
			if [ -d "${icontype}" ]
			then
				echo "> ${icontype}"
				cd "${icontype}"
				for pngname in *
				do
					iconname=$(basename ${pngname} ".png")
					echo ".icon-${iconsize} .${icontype}-${iconname} { background-image: url(\"${icontype}/${pngname}\"); }" >> ../icons.css
				done
				cd ..
			fi
		done
		cd ..
	done
}


createSkeleton

checkOut

copyIcons

createCssFiles



echo "The icon theme has been successfully created in $DEST_DIR"
exit 0

